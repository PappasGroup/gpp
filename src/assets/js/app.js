$(document).foundation();

// DISCLAIMER TOGGLE
$('.disclaimer-toggle').click(function(){
   $(this).toggleClass('flip').next().slideToggle();
});

// SMOOTH SCROLLING
$(document).on('click', 'a[href*="#"]:not([href="#"])', function(e) {
   if (location.pathname.replace(/^\//,'') === this.pathname.replace(/^\//,'') && location.hostname === this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
         $('html, body').animate({
            scrollTop: target.offset().top
         }, 1000);
         e.preventDefault();
      }
   }
});

//BACK TO TOP SMOOTH SCROLLING
$("a[href='#top']").click(function() {
   $("html, body").animate({ scrollTop: 0 }, 1000);
   return false;
});

// ACCORDION SUB MENU TRIGGER CLOSE
$('.close-accordion a').click(function(e){
   e.preventDefault();
   $(this).parent().parent().removeClass('is-active').attr('aria-hidden', 'true').slideToggle();
   $(this).parent().parent().parent().attr('aria-expanded', 'false');
});

// MOBILE MENU
$(document).ready(function(){
   $('.mobile-nav').click(function(){
      $(this).toggleClass('open');
   });
});